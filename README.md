# IP Config

A service that exposes IP configuration data. 

## Prerequisites
- [Java 8+](https://java.com/de/download)
- [Maven 3.x+](https://maven.apache.org/download.cgi)

## Run

To run the application locally use:
```
mvn spring-boot:run
```

or
```
mvn clean package
java -jar target/ip-config-0.0.1-SNAPSHOT.jar
```

The application will be available at http://localhost:8080 with the following endpoint: http://localhost:8080/ip-configs.

## Test

To trigger unit and integration tests run:

```
mvn clean verify
```

## Package
To create the application package within `target/` directory run:

```
mvn clean package
```


# Installation:

## For testing and development purposes.
``` 
Build :
        $ docker build -t ip-config .
Deploy:
        $ docker-compose up -d
```

## GitLab CI/CD

* Auto-build and Auto-deploy is achieved with ".gitlab-ci.yml".
* Refer the above file to have a look at stages mentioned. (Compile >> Test >> Docker Image Build >> Docker Container startup)

## Rancher

`docker-compose.yaml` and `rancher-compose.yaml` is present under `rancher` directory to deploy our application on Rancher platform with production grade features such as below:<br/>
```
a) At least 2 instances of the application are running at the same time.
b) A load balancer is present.
c) Only one service container of the application must run per host.
```       
