FROM maven:3.6.3-jdk-8
WORKDIR /app

ARG TARGET_JAR=ip-config-0.0.1-SNAPSHOT.jar
ENV TARGET_JAR=${TARGET_JAR}

ADD target/${TARGET_JAR}  /app/.

EXPOSE 8080

CMD ["/bin/sh", "-c", "java -jar /app/$TARGET_JAR"]